
let input = 
`A2727 410
78888 626`.split("\n") //replace with whatever the actual input is

input = input.map(e => e.split(" "))

var handRank = (hand) => {
    hand = hand.split('')
    let handObj = {}
    
    hand.map(e => handObj[e] = handObj[e] ? handObj[e] + 1 : 1)
     
    let keys = Object.keys(handObj)
    let matches = [0,0,0,0,0,0]
    let numJ = 0
    
    numJ = handObj["J"] ? handObj["J"] : 0
    
    if (numJ >= 4) {
        matches[5]++
        numJ = 0
    }
    
    for (var j = 0; j < keys.length; j++){
        if (keys[j] == "J") {
            //don't add extra to J
        } else {
            matches[handObj[keys[j]]]++
        }
    }
    
    if (numJ >= 1){
        if (matches[5-numJ]) {
            matches[5-numJ]--
            matches[5]++
        } else if (matches[4 - numJ]) {
            matches[4 - numJ]--
            matches[4]++
        } else if (matches[3 - numJ]) {
            matches[3-numJ]--
            matches[3]++
        } else {
            matches[2]++
        }
    } 

    return "" + matches[5] + matches[4] + matches[3] + matches[2]
}

for (var i = 0; i < input.length; i++){
    let hand = input[i][0]
    let bid = input[i][1]
    input[i][2] = +handRank(hand)
}

let converter = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "J": 1,
    "Q": 12,
    "K": 13,
    "A": 14
}

input = input.sort((a,b) => {
    if (a[2] > b[2]) return 1
    else if (a[2] < b[2]) return -1
    else {
        a = a[0].split("").map(e => converter[e])
        b = b[0].split("").map(e => converter[e])
        for (var i = 0; i < a.length; i++){
            if (a[i] > b[i]) return 1
            else if (a[i] < b[i]) return -1
        }
        return 0
    }
})

let result = 0

for (var i = 0; i < input.length; i++){
    result += +input[i][1] * (i + 1)
}

console.log(result)
